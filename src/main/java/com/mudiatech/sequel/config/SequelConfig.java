package com.mudiatech.sequel.config;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.mudiatech.sequel.sql.LimitFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class SequelConfig {

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTEmplate(JdbcTemplate jdbcTemplate) {
        return new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Bean
    public LimitFactory limitFactory(DatabaseMetaData databaseMetaData) throws SQLException {
        return new LimitFactory(databaseMetaData.getDatabaseProductName());
    }

    @Bean
    public DatabaseMetaData databaseMetaData(DataSource dataSource) throws SQLException {
        long startTime = System.currentTimeMillis();
        DatabaseMetaData metadata = dataSource.getConnection().getMetaData();
        log.debug("Total time getting database metadata: {} ms", System.currentTimeMillis() - startTime);
        return metadata;
    }

}
